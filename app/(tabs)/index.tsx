import { Pressable, StyleSheet } from "react-native"
import { Image } from "expo-image"
import { View, Text } from "@/components/Themed"
import { Link, router } from "expo-router"

export default function TabOneScreen() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../../assets/images/jon.jpeg")}
        contentFit="cover"
        transition={1000}
      />
      <Text>Jonathan Lagneaux</Text>
      <Text>
        Titre : Une librairie de composants Android/iOS partageable
        via une simple page WEB !
      </Text>
      <Text>
        Jonathan nous présentera une innovation permettant de partager
        une librairie de composants Android/iOS au travers d'une
        simple page web, une approche révolutionnaire qui pourrait
        transformer votre façon de développer et partager des
        composants entre plateformes.
      </Text>
      <Pressable
        onPress={() => {
          router.push({
            pathname: "/modal",
            params: { speaker: "jon" },
          })
        }}
      >
        <Text>Feedback</Text>
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    gap: 15,
  },
  image: {
    width: 100,
    height: 100,
  },
})
