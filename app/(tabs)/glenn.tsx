import { Pressable, StyleSheet } from "react-native"
import { Image } from "expo-image"
import { View, Text } from "@/components/Themed"
import { Link, router } from "expo-router"

export default function TabTwoScreen() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../../assets/images/glenn.jpeg")}
        contentFit="cover"
        transition={1000}
      />
      <Text>Glenn Guegan</Text>
      <Text>Titre : Expo est il une solution viable en 2024 ?</Text>
      <Text style={styles.content}>
        Explorez l'écosystème d'Expo, un framework clé pour le
        développement d'applications natives avec React. Glenn nous
        partagera une analyse approfondie des évolutions récentes,
        notamment EAS (Expo Application Services), et discutera des
        avantages et inconvénients d'Expo, ainsi que de sa pertinence
        pour vos projets actuels et futurs.
      </Text>
      <Pressable
        onPress={() => {
          router.push({
            pathname: "/modal",
            params: { speaker: "glenn" },
          })
        }}
      >
        <Text>Feedback</Text>
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    gap: 15,
  },
  image: {
    width: 100,
    height: 100,
  },
  content: {
    paddingHorizontal: 20,
  },
})
