import { Pressable, StyleSheet } from "react-native"
import { Image } from "expo-image"
import { View, Text } from "@/components/Themed"
import { Link, router } from "expo-router"

export default function TabTwoScreen() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../../assets/images/jeremy.jpeg")}
        contentFit="cover"
        transition={1000}
      />
      <Text>Jeremy Riverain</Text>
      <Text>Titre : L’exécution de code natif avec Flutter</Text>
      <Text>
        Découvrez avec nous les secrets de Flutter et comment ce
        framework permet une interaction fluide avec les
        fonctionnalités natives du téléphone. Jeremy nous guidera à
        travers les mécanismes tels que Method Channel, Pigeon et FFI,
        en expliquant leur utilité, leurs cas d’usage et comment les
        mettre en œuvre efficacement.
      </Text>
      <Pressable
        onPress={() => {
          router.push({
            pathname: "/modal",
            params: { speaker: "jeremy" },
          })
        }}
      >
        <Text>Feedback</Text>
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    gap: 15,
  },
  image: {
    width: 100,
    height: 100,
  },
})
