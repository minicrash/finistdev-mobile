import { Link, router, useLocalSearchParams } from "expo-router"
import { useEffect } from "react"
import { Linking, Platform, Pressable } from "react-native"
import { WebView } from "react-native-webview"
import { Text } from "@/components/Themed"

const getOpenFeebackUrl = (speaker: string) => {
  switch (speaker) {
    case "jon":
      return "https://openfeedback.io/LlmHR2EZeXjlrWmR01LW/2024-02-22/1W03s00eHeByxhxiNxMo"

    case "glenn":
      return "https://openfeedback.io/LlmHR2EZeXjlrWmR01LW/2024-02-22/BoPuqxOmxp6T2Qq1SaLR"

    case "jeremy":
      return "https://openfeedback.io/LlmHR2EZeXjlrWmR01LW/2024-02-22/3WoEDpEIU2yDkk0Pwtr5"

    default:
      return "https://openfeedback.io/LlmHR2EZeXjlrWmR01LW/2024-02-22"
  }
}

export default function ModalScreen() {
  const params = useLocalSearchParams()

  return (
    <>
      {Platform.OS === "web" ? (
        <Pressable
          onPress={() =>
            Linking.openURL(
              getOpenFeebackUrl(params.speaker as string)
            )
          }
        >
          Open Feedback
        </Pressable>
      ) : (
        <WebView
          source={{
            uri: getOpenFeebackUrl(params.speaker as string),
          }}
        />
      )}
    </>
  )
}
